# libudev-compat

An ABI-compatible libudev that does not need udevd to be running.

This library is a fork of [EUDEV](https://wiki.debian.org/Apt) at the same a fork of git://anongit.freedesktop.org/systemd/systemd with the aim of isolating udev from any particular flavor of system initialization.

