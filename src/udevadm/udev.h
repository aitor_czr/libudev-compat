/*
 * udev.h
 * 
 * Forked from eudev/src/udevadm/udev.h
 * 
 * All modifications to the original source file are:
 *  - Copyright (C) 2022 Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org> 
 *
 * Original copyright and license text produced below.
 */

/*
 * Copyright (C) 2003 Greg Kroah-Hartman <greg@kroah.com>
 * Copyright (C) 2003-2010 Kay Sievers <kay@vrfy.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <sys/sysmacros.h>
#include <sys/types.h>
#include <sys/param.h>
#include <signal.h>

#include "macro.h"
#include "libudev.h"
#include "libudev-private.h"
#include "util.h"
#include "label.h"
#include "strv.h"

struct udev_event {
        struct udev *udev;
        struct udev_device *dev;
        struct udev_device *dev_parent;
        struct udev_device *dev_db;
        char *name;
        char *program_result;
        mode_t mode;
        uid_t uid;
        gid_t gid;
        struct udev_list seclabel_list;
        struct udev_list run_list;
        int exec_delay;
        usec_t birth_usec;
        int fd_signal;
        unsigned int builtin_run;
        unsigned int builtin_ret;
        bool sigterm;
        bool inotify_watch;
        bool inotify_watch_final;
        bool group_set;
        bool group_final;
        bool owner_set;
        bool owner_final;
        bool mode_set;
        bool mode_final;
        bool name_final;
        bool devlink_final;
        bool run_final;
};

struct udev_watch {
        struct udev_list_node node;
        int handle;
        char *name;
};

/* built-in commands */
enum udev_builtin_cmd {
#ifdef HAVE_BLKID
        UDEV_BUILTIN_BLKID,
#endif
        UDEV_BUILTIN_BTRFS,
        UDEV_BUILTIN_HWDB,
        UDEV_BUILTIN_INPUT_ID,
        UDEV_BUILTIN_KEYBOARD,
#ifdef HAVE_KMOD
        UDEV_BUILTIN_KMOD,
#endif
        UDEV_BUILTIN_NET_ID,
        UDEV_BUILTIN_PATH_ID,
        UDEV_BUILTIN_USB_ID,
        UDEV_BUILTIN_MAX
};
struct udev_builtin {
        const char *name;
        int (*cmd)(struct udev_device *dev, int argc, char *argv[], bool test);
        const char *help;
        int (*init)(struct udev *udev);
        void (*exit)(struct udev *udev);
        bool (*validate)(struct udev *udev);
        bool run_once;
};
extern const struct udev_builtin udev_builtin_btrfs;
extern const struct udev_builtin udev_builtin_hwdb;
extern const struct udev_builtin udev_builtin_input_id;
extern const struct udev_builtin udev_builtin_keyboard;
extern const struct udev_builtin udev_builtin_net_id;
extern const struct udev_builtin udev_builtin_path_id;
extern const struct udev_builtin udev_builtin_usb_id;

void udev_builtin_init(struct udev *udev);
void udev_builtin_exit(struct udev *udev);
enum udev_builtin_cmd udev_builtin_lookup(const char *command);
const char *udev_builtin_name(enum udev_builtin_cmd cmd);
bool udev_builtin_run_once(enum udev_builtin_cmd cmd);
int udev_builtin_run(struct udev_device *dev, enum udev_builtin_cmd cmd, const char *command, bool test);
void udev_builtin_list(struct udev *udev);
bool udev_builtin_validate(struct udev *udev);
int udev_builtin_add_property(struct udev_device *dev, bool test, const char *key, const char *val);
int udev_builtin_hwdb_lookup(struct udev_device *dev, const char *prefix, const char *modalias,
                             const char *filter, bool test);

/* udevadm commands */
struct udevadm_cmd {
        const char *name;
        int (*cmd)(struct udev *udev, int argc, char *argv[]);
        const char *help;
        int debug;
};
extern const struct udevadm_cmd udevadm_info;
extern const struct udevadm_cmd udevadm_trigger;
extern const struct udevadm_cmd udevadm_settle;
extern const struct udevadm_cmd udevadm_control;
extern const struct udevadm_cmd udevadm_monitor;
extern const struct udevadm_cmd udevadm_hwdb;
extern const struct udevadm_cmd udevadm_test;
extern const struct udevadm_cmd udevadm_test_builtin;
